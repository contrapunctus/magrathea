* The Strata Document Format
This document defines a WYSIWYG document format for the Magrathea environment.

It has the following goals -
1. power equivalent to traditional WYSIWYM markups, e.g. LaTeX
2. simple to understand and implement
3. safety of opening and editing documents
4. strict separation of semantics and presentation, content and structure

Some features of note -
1. built-in versioning with awareness of each document entity
2. "layers" - a way to non-destructively add changes to a document
3. the ability to link to or reuse any document object

# The relationship between document objects and layouts sounds like libraries and UIs...so maybe there's no need to have multiple layouts in a single document, and maybe I ought to be thinking about a more general mechanism to let independent documents reuse document objects from each other?

# How might formatted text be copy-pasted between applications? Would we send the entire style information along? Maybe just the /applicable/ style information, if possible?

** Components
Each document contains four components -
1. a collection of document objects
2. style information
3. a collection of layers
4. a layout, specifying order of layers and how document objects are organized to form the document structure

*** Document objects
Document objects represent the content of a document.
1. primitive objects - text, image
   # audio, video?
2. container objects e.g. a song contains stanzas; a stanza contains lines; a chord or melody contains notes

Each object is assigned a UID.

Any object may be assigned a =name=, which is a string with a name meaningful to the user.

# Is the name unique in the document? The idea is to create human-readable links. Is it even necessary?

**** Standard container objects
1. Paragraph
2. Heading
3. Emphasis
4. Quote
5. Footnote
6. Cell
7. Row
8. Column
9. Table
10. Link
    * the =target= property contains a URL (external links) or an object's UID (internal links)
11. Template (consumes a fixed number of objects as arguments and uses them in a predetermined combination)

New object types may be added by implementations and end-users as they see fit. Layers expect specific objects, though, and may need to be updated to handle the new objects.

The following sections list standard objects for various use-cases.

***** Collections
1. Table of contents
2. Index
3. Bibliography

***** Literature
1. Chapter
2. Verse/Stanza
3. Poem
4. Song

***** Literate programming
1. Identifier
2. Source Block
   * properties - tangle on/off/output file, etc

***** Sheet music
1. Note
2. Chord
3. Voice
4. Staff
5. System

***** Theatre script
1. character
   * content - name to be displayed in dialogues
   * =dramatis= - name to be displayed in the dramatis personae. If omitted, content is used.
   * =description=
2. Dialogue
3. Stage direction
4. Act
5. Dramatis personae

***** Culinary recipes
1. Yield
2. Time
3. Ingredient list
4. Ingredient
5. Instructions
6. Instruction
7. Nutritional information

**** Standard object metadata fields (Dublin Core)
Most of these can apply to any document object, not just the document itself, e.g. a document may contain text in multiple languages, from different contributors, who may desire different copyright terms - a single root-level field for these would not be accurate.

1. Contributor
2. Coverage
3. Creator
4. Date
5. Description
6. Format
7. Identifier – "An unambiguous reference to the resource within a given context". (?)
8. Language
9. Publisher
10. Relation – "A related resource". (?)
11. Rights
12. Source
13. Subject
14. Title (?)
15. Type – "The nature or genre of the resource".

*** Layouts
A layout puts document objects and layers together to form the document. It consists of a document tree and a list of layers.

The document tree determines the structure of the document, and the relationship between nodes (which nodes contain which). It may refer to the same document object any number of times, facilitating removal of duplication.

Layouts also specify the document class. Some examples -
1. letter
2. CV/resumé
3. book
4. article
5. poster
6. presentation
7. pamphlet
8. theatre script/film screenplay
9. music score

New document classes may be added by implementations and end-users as they see fit.

Layouts do not describe how the document will be displayed. For that we turn to /layers/.

# Document classes just seem to serve as the base style layer...do they really need to be a separate concept?
# How about higher-level container objects representing what we think of as document classes? e.g. literate program, sheet music, letter, book, dramatic script...? That would give us embeddable document classes, which I initially wanted, but later abandoned because it wasn't used too often.

*** Style
# the default style, acting as the base for styling layers
# largely inspired by CSS

Selectors
1. ID - select specific object
2. type - select objects of that type
3. property - select objects with that property
4. property value - select objects with that property-value

*** Layers
Layers are used to make changes to an existing document while keeping them separate from previous changes, akin to layers in a raster image editing program.

Layers come in two varieties -
1. /content layers/, containing additions/modifications to the content of the default layout, or of other content layers.
2. /styling layers/, containing presentation information for the default layout or content layers.

Each layer has an =applies-to= property with a value of either a layer ID (to apply this layer to the specified layer) or =t= (to act as the base layer). Any layer can be applied to any other layer coming before it in the layout layer list. The first layer in the list must be the base layer. There can only be one base layer in a document.
# What happens when the first layer has an =applies-to= property?

**** Content layers
# How to refer to another layer
# Inserting data at a given point in the document
# Modifying objects in another layer

**** Styling layers
Styling layers specify how the content of layouts or layers is to be presented.

Display properties controlled by styling layers include -
1. font style, size, family
2. underline or strikethrough
3. page size


** Programmatic creation of components
All four components in the previous section can be created programmatically. When programmatic construction of a document object, layer, layout, or style information is desired, instead of the data structure forming the value for such a component, a reference to a source code document object can be provided instead. The source code is evaluated by the underlying Lisp implementation just before the document is displayed.

The advantage of using source code objects for this purpose is that they can also be displayed within the document; thus, the source code being used to create the document may also be part of the document.

# TODO - examples

# While I'm writing with the assumption that the source code will be Lisp, it may be possible to evaluate source code in /any/ language to create document components programmatically (as long as the result is a suitable s-expression), similar to Org. That would be rather cool.

** Schema
A Strata document is a text file with extension ".str", containing s-expressions readable by the Common Lisp reader.

# The Common Lisp reader has a construct which evaluates code at read time, which we don't want. It can be disabled by Strata implementers, or we can use some other format...such as EDN.

#+BEGIN_SRC lisp
(strata
 ((version . "<major>.<minor>.<patch>")
  <property>*)

 (objects
  (<object UID>
           (<object-type>
            ((version . <version>)
             <property>*)
            <content>)
           ...)
  ...)

 (layers
  (<layer UID>
          (<layer-type>
           ((version . <version>)
            (applies-to . <layer UID>)
            <property>*)
           <content>)
          ...)
  ...)

 (layout
  (<version> (<property>*)
             (tree <document tree>)
             (layers <layer UID>*))
  ...))
#+END_SRC

<property> ::= (key . value)

<layer-type> ::= content | styling

Both =...= and =*= mean "the preceding element is optional and may be repeated any number of times."

# Compression is omitted, because we don't want compression/decompression taking up time on save/load.

<version> is an integer which is incremented by 1 for each new version of the object/layer/layout.

# Add a required timestamp field for each versioned entity?

# What about branches? :\

# Switch to an XML-style =(name ((<property> . <value>)*) content)= schema for everything? 🤔
# * Can't have content length in a single expression

** Examples
*** theatre script
#+BEGIN_SRC lisp
(strata
 ((version . "<major>.<minor>.<patch>")
  (description . "The Project Gutenberg eBook, The Importance of Being Earnest, by Oscar Wilde")
  (rights . "This eBook is for the use of anyone anywhere at no cost and with almost no restrictions whatsoever.  You may copy it, give it away or re-use it under the terms of the Project Gutenberg License included with this eBook or online at www.gutenberg.org")
  (title . "The Importance of Being Earnest")
  (subtitle . "A Trivial Comedy for Serious People")
  (author . "Oscar Wilde")
  (date . "2006-08-29")
  (language . "eng")
  (ebook . 844)
  (source . "1915 Methuen & Co. Ltd. edition")
  (transcriber . "David Price")
  (email . "ccx074@pglaf.org"))
 (objects
  (1 (text ((version . 1)) "The Persons in the Play"))
  (2 (character
      ((version . 1)
       (dramatis . "John Worthing, J.P."))
      "Jack"))
  (3 (character
      ((version . 1)
       (dramatis . "Algernon Moncrieff"))
      "Algernon"))
  (4 (character
      ((version . 1)
       (dramatis . "Rev. Canon Chasuble, D.D."))
      "Chasuble"))
  (5 (character
      ((version . 1)
       (dramatis . "Merriman, Butler"))
      "Merriman"))
  (6 (character
      ((version . 1)
       (dramatis . "Lane, Manservant"))
      "Lane"))
  (7 (character
      ((version . 1))
      "Lady Bracknell"))
  (8 (character
      ((version . 1)
       (dramatis . "Hon. Gwendolen Fairfax"))
      "Gwendolen"))
  (9 (character
      ((version . 1) (dramatis . "Cecily Cardew"))
      "Cecily"))
  (10 (character
       ((version . 1) (dramatis . "Miss Prism, Governess"))
       "Miss Prism"))
  (11 (text ((version . 1)) "The Scenes of the Play"))
  (12 (text ((version . 1)) "ACT I.  Algernon Moncrieff's Flat in Half-Moon Street, W."))
  (13 (text ((version . 1)) "ACT II.  The Garden at the Manor House, Woolton."))
  (14 (text ((version . 1)) "ACT III.  Drawing-Room at the Manor House, Woolton."))
  (15 (text ((version . 1)) "TIME: The Present.")))

 (layers
  (<layer UID>
          (<version> ((type . <layer-type>)
                      <property>*)
                     <content>)
          ...)
  ...)

 (layout
  (1 nil
     (tree title
           subtitle
           (dramatis (title 1) 2 3 4 5 6 7 8 9 10)
           (scenes (title 11) 12 13 14 15))
     (layers <layer UID>*))
  ...))
#+END_SRC

*** music score
** Future
Scripting within a document enables many use cases, but with some prominent risks -
1. it may use more computational resources than the user likes
2. it may access the disk and/or network to violate user privacy
3. it tends to create ad hoc and inconsistent solutions instead of standardized, consistent ones.

A future version of Strata may support embedded scripts, with implementations using sandboxing to provide guarantees against #1 and #2.
