(defsystem     magrathea
  :version     "0.0.1"
  :serial      t
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :depends-on  (:mcclim)
  :components  ((:file "src/file-manager")))
