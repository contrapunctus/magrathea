(in-package :cl)

(defpackage :magrathea.file-manager
  (:use :clim :clim-lisp)
  (:import-from :uiop :directory-files :enough-pathname)
  (:export :run-file-manager))

(in-package :magrathea.file-manager)

(defclass file ()
  ((path :initarg :pathname :accessor path)))

(defclass dir (file) nil)

(define-application-frame file-manager ()
  ((files :initform (directory-files *default-pathname-defaults*)
          :accessor files))
  (:pointer-documentation t)
  (:panes (app :application
               :display-time t
               :height 300 :width 600
               :display-function 'display-files)
          (int :interactor :height 200 :width 600))
  (:layouts (default (vertically () app int))))

(defun display-files (frame pane)
  ;; display each
  (loop for path in (files frame)
        do (with-output-as-presentation
               (pane path 'file)
             (format pane "~a~%"
                     (enough-pathname path
                                      *default-pathname-defaults*)))))

(define-file-manager-command (com-quit :name t) ()
  (frame-exit *application-frame*))

(defun run-file-manager ()
  (run-frame-top-level (make-application-frame 'file-manager)))
